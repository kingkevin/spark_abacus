/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/** library to get time from a DCF77 module (API)
 *  @file rtc_dcf77.h
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @date 2016
 *  @note peripherals used: GPIO @ref rtc_dcf77_gpio, timer @ref rtc_dcf77_timer 
 */
#pragma once

/** @defgroup rtc_dcf77_gpio output to enable DCF module and input to capture DCF signal
 *  @{
 */
#define RTC_DCF77_ENABLE_RCC RCC_GPIOA /**< GPIO peripheral clock to enable the module */
#define RTC_DCF77_ENABLE_PORT GPIOA /**< GPIO port to enable the module */
#define RTC_DCF77_ENABLE_PIN GPIO2 /**< GPIO pinto enable the module */
#define RTC_DCF77_SIGNAL_RCC RCC_GPIOA /**< GPIO peripheral clock to capture the DCF signal */
#define RTC_DCF77_SIGNAL_PORT GPIOA /**< GPIO port to capture the DCF signal */
#define RTC_DCF77_SIGNAL_PIN GPIO3 /**< GPIO pin to capture the DCF signal */
#define RTC_DCF77_SIGNAL_EXTI EXTI3 /**< GPIO external interrupt to capture the DCF signal */
#define RTC_DCF77_SIGNAL_IRQ NVIC_EXTI3_IRQ /**< GPIO line interrupt */
#define RTC_DCF77_SIGNAL_ISR exti3_isr /**< GPIO line interrupt service routine */
/** @} */

/** @defgroup rtc_dcf77_timer timer to measure signal puls
 *  @{
 */
#define RTC_DCF77_TIMER TIM4 /**< timer peripheral */
#define RTC_DCF77_TIMER_RCC RCC_TIM4 /**< timer peripheral clock */
#define RTC_DCF77_TIMER_MAX_TIME 2200 /**< the maximum time in ms the timer can count. DCF77 have pulses < 2s */
/** @} */

/** set when time information has been received */
extern volatile bool rtc_dcf77_time_flag;

/** setup DCF77 time receiver module */
void rtc_dcf77_setup(void);
/** switch on DCF77 time receiver module */
void rtc_dcf77_on(void);
/** switch off DCF77 time receiver module */
void rtc_dcf77_off(void);
/** get last received DCF77 time
 *  @return array of {minutes (00-49), hours (00-23), date (01-31), day of the week (1-7=Monday-Sunday), month (01-12), year of the century (00-99)} if received time is valid, NULL else
 */
uint8_t* rtc_dcf77_time(void);
