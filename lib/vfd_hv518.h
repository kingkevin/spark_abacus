/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/** library to drive vacuum fluorescent display using supertex HV518 shift register VFD drivers (API)
 *  @details the current configuration is for a VFD extracted from a Samsung SER-6500 cash register
 *  @file vfd_hv518.h
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @date 2016
 *  @note peripherals used: USART @ref usart
 */

/** number HV518 VFD drivers */
#define VFD_DRIVERS 3
/** number of digits blocks on SER-6500 VFD */
#define VFD_DIGITS 10
/** number of dot matrix blocks on SER-6500 VFD */
#define VFD_MATRIX 12

/** set character to digit block
 *  @param[in] nb digit block to set
 *  @param[in] c ASCII character to set
 *  @note use the MSB of @p nb to enable the dot
 */
void vfd_digit(uint8_t nb, char c);
/** set character to matrix block
 *  @param[in] nb matrix block to set
 *  @param[in] c ASCII character to set
 *  @note on ASCII characters are used for pictures
 */
void vfd_matrix(uint8_t nb, char c);
/** clear VFD display */
void vfd_clear(void);
/** test VFD display (light up all segments) */
void vfd_test(void);
/** switch VFD on */
void vfd_on(void);
/** switch VFD display off */
void vfd_off(void);
/** setup VFD */
void vfd_setup(void);
