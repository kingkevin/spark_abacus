/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/** library to query measurements from eastron SDM120-ModBus electricity meter (API)
 *  @file sensor_sdm120.h
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @date 2016
 *  @note peripherals used: USART @ref sensor_sdm120_usart , GPIO @ref sensor_sdm120_gpio , timer @ref sensor_sdm120_timer
 */
#pragma once

/** a measurement response has been received */
extern volatile bool sensor_sdm120_measurement_received;

/** measurement types offered by electricity meter in 3xxx input registers */
enum sensor_sdm120_measurement_type_t {
	SENSOR_SDM120_VOLTAGE = 0,
	SENSOR_SDM120_CURRENT,
	SENSOR_SDM120_POWER_ACTIVE,
	SENSOR_SDM120_POWER_APPARENT,
	SENSOR_SDM120_POWER_REACTIVE,
	SENSOR_SDM120_POWER_FACTOR,
	SENSOR_SDM120_FREQUENCY,
	SENSOR_SDM120_ENERGY_ACTIVE_IMPORT,
	SENSOR_SDM120_ENERGY_ACTIVE_EXPORT,
	SENSOR_SDM120_ENERGY_REACTIVE_IMPORT,
	SENSOR_SDM120_ENERGY_REACTIVE_EXPORT,
	SENSOR_SDM120_ENERGY_ACTIVE_TOTAL,
	SENSOR_SDM120_ENERGY_REACTIVE_TOTAL,
	SENSOR_SDM120_MEASUREMENT_MAX
};

/** configuration types for electricity meter in 4xxx holding registers */
enum sensor_sdm120_configuration_type_t {
	SENSOR_SDM120_RELAY_PULSE_WIDTH = 0,
	SENSOR_SDM120_NETWORK_PARITY_STOP,
	SENSOR_SDM120_METER_ID,
	SENSOR_SDM120_BAUD_RATE,
	SENSOR_SDM120_PULSE_1_OUTPUT_MODE,
	SENSOR_SDM120_TIME_OF_SCROLL_DISPLAY,
	SENSOR_SDM120_PULSE_1_OUTPUT,
	SENSOR_SDM120_MEASUREMENT_MODE,
	SENSOR_SDM120_CONFIGURATION_MAX
};

/** setup peripherals to communicate with electricity meter
 *  @param[in] baudrate baud rate of RS485 serial communication
 */
void sensor_sdm120_setup(uint32_t baudrate);
/** request measurement from electricity meter
 *  @param[in] meter_id electricity meter device ID
 *  @param[in] type measurement type to request
 *  @return if transmission started
 */
bool sensor_sdm120_measurement_request(uint8_t meter_id, enum sensor_sdm120_measurement_type_t type);
/** request configuration from electricity meter
 *  @param[in] meter_id electricity meter device ID
 *  @param[in] type configuration type to request
 *  @return if transmission started
 */
bool sensor_sdm120_configuration_request(uint8_t meter_id, enum sensor_sdm120_configuration_type_t type);
/** set configuration in electricity meter
 *  @param[in] meter_id electricity meter device ID
 *  @param[in] type configuration type to set
 *  @param[in] value configuration value to set
 *  @return if transmission started
 */
bool sensor_sdm120_configuration_set(uint8_t meter_id, enum sensor_sdm120_configuration_type_t type, float value);
/** decode received measurement
 *  @return decoded measurement or number of registers written, NaN if message has error or no new measurement has been received, infinity if an error or unknown message has been received
 */
float sensor_sdm120_measurement_decode(void);
