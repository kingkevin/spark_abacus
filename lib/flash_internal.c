/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/** library to read/write internal flash (code)
 *  @file flash_internal.c
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @date 2016
 *  @note peripherals used: none
 */
/* standard libraries */
#include <stdint.h> // standard integer types
#include <stdlib.h> // general utilities

/* STM32 (including CM3) libraries */
#include <libopencm3/stm32/desig.h> // device signature utilities
#include <libopencm3/stm32/flash.h> // flash utilities

#include "flash_internal.h" // flash storage library API
#include "global.h" // global definitions

/** the flash page size (medium-density devices have 1KiB page size) */
#define PAGE_SIZE 1024

bool flash_internal_read(uint32_t address, uint8_t *buffer, size_t size)
{
	// verify it's in the storage area
	if (address<STORAGE_START || (address+size)>STORAGE_END) {
		return false;
	}
	if (buffer==NULL || size==0) {
		return false;
	}

	// copy data byte per byte
	// a more efficient way would be to copy words, than the remaining bytes
	for (size_t i=0; i<size; i++) {
		buffer[i] = *((uint8_t*)address+i);
	}

	return true;
}

bool flash_internal_write(uint32_t address, uint8_t *buffer, size_t size)
{
	// verify it's in the storage area
	if (address<STORAGE_START || (address+size)>STORAGE_END) {
		return false;
	}
	if (buffer==NULL || size==0) {
		return false;
	}

	uint8_t page[PAGE_SIZE]; // the complete page to write

	flash_unlock(); // unlock flash to be able to write it
	// go through memory
	while (size) {
		uint32_t page_pre = address%PAGE_SIZE; // the beginning data size in the page
		address -= page_pre; // go to beginning of the page
		storage_read(address, &page[0], page_pre); // copy existing data
		if (size>=PAGE_SIZE-page_pre) { // no need to read tailing page data
			for (uint16_t i=0; i<PAGE_SIZE-page_pre; i++) { // put buffer in page
				page[page_pre+i] = buffer[i];
			}
			buffer += PAGE_SIZE-page_pre; // adjust remaining buffer
			size -= PAGE_SIZE-page_pre; // adjust remaining size
		} else { // need read tailing page data
			for (uint16_t i=0; i<size; i++) { // put buffer in page
				page[page_pre+i] = buffer[i];
			}
			buffer += size; // adjust remaining buffer
			storage_read(address+page_pre+size, &page[page_pre+size], PAGE_SIZE-page_pre-size); // read tailing page data
			size = 0; // adjust remaining size
		}

		// write page
		flash_erase_page(address); // erase current page
		if (flash_get_status_flags()!=FLASH_SR_EOP) { // operation went wrong
			flash_lock(); // lock back flash to protect it
			return false;
		}
		for (uint16_t i=0; i<PAGE_SIZE/2; i++) { // write whole page
			flash_program_half_word(address+i*2, *((uint16_t*)page+i));
			if (flash_get_status_flags()!=FLASH_SR_EOP) { // operation went wrong
				flash_lock(); // lock back flash to protect it
				return false;
			}
			if (*((uint16_t*)address+i)!=*((uint16_t*)page+i)) { // verify the programmed data is right
				flash_lock(); // lock back flash to protect it
				return false;
			}
		}

		address += PAGE_SIZE; // go to next page

	}
	flash_lock(); // lock back flash to protect it

	return true;
}
