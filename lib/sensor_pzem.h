/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/** library to query measurements from peacefair PZEM-004 and PZEM-004T electricity meter (API)
 *  @file sensor_pzem.h
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @date 2016
 *  @note peripherals used: USART @ref sensor_pzem_usart
 */
#pragma once

/** a measurement response has been received */
extern volatile bool sensor_pzem_measurement_received;

/** measurements (and configurations) offered by electricity meter */
enum sensor_pzem_measurement_type_t {
	SENSOR_PZEM_VOLTAGE = 0,
	SENSOR_PZEM_CURRENT = 1,
	SENSOR_PZEM_POWER = 2,
	SENSOR_PZEM_ENERGY = 3,
//	SENSOR_PZEM_ADDRESS = 4, // this is a setting, not a measurement
//	SENSOR_PZEM_ALARM = 5, // this is a setting, not a measurement
	SENSOR_PZEM_MAX
};

/** measurement returned by electricity meter */
struct sensor_pzem_measurement_t {
	enum sensor_pzem_measurement_type_t type; /**< measurement type */
	bool valid; /**< is the measurement valid (e.g. format and checksum are correct) */
	/** possible measurement values */
	union measurement_t {
		float voltage; /**< measured voltage in volts */
		float current; /**< measured current in amperes */
		uint16_t power; /**< measured power in watts */
		uint32_t energy; /**< measured energy in watts/hour (24 bits) */
	} value; /**< measurement value */
};

/** setup peripherals to communicate with electricity meter */
void sensor_pzem_setup(void);
/** request measurement from electricity meter
 *  @param[in] address electricity meter device address
 *  @param[in] type measurement type to request
 */
void sensor_pzem_measurement_request(uint32_t address, enum sensor_pzem_measurement_type_t type);
/** decode received measurement
 *  @return decoded measurement (invalid if no new measurement has been received)
 */
struct sensor_pzem_measurement_t sensor_pzem_measurement_decode(void);
