/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/** library to send data using ESP8266 WiFi SoC (API)
 *  @file radio_esp8266.h
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @date 2016
 *  @note peripherals used: USART @ref radio_esp8266_usart
 */
#pragma once

/** a response has been returned by the radio */
extern volatile bool radio_esp8266_activity;
/** the last command has succeeded */
extern volatile bool radio_esp8266_success;

/** setup peripherals to communicate with radio
 *  @note this is blocking to ensure we are connected to the WiFi network
 */
void radio_esp8266_setup(void);
/** establish TCP connection
 *  @param[in] host host to connect to
 *  @param[in] port TCP port to connect to
 *  @note wait for activity to get success status
 */
void radio_esp8266_tcp_open(char* host, uint16_t port);
/** send data (requires established connection)
 *  @param[in] data data to send
 *  @param[in] length size of data to send
 *  @note wait for activity to get success status
 */
void radio_esp8266_send(uint8_t* data, uint8_t length);
/** close established connection
 *  @note wait for activity to get success status
 */
void radio_esp8266_close(void);
