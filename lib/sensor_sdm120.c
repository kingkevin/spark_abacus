/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/** library to query measurements from eastron SDM120-ModBus electricity meter (code)
 *  @file sensor_sdm120.c
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @date 2016
 *  @note peripherals used: USART @ref sensor_sdm120_usart , GPIO @ref sensor_sdm120_gpio , timer @ref sensor_sdm120_timer
 */
/* standard libraries */
#include <stdint.h> // standard integer types
#include <stdlib.h> // general utilities
#include <math.h> // mathematical utilities

/* STM32 (including CM3) libraries */
#include <libopencm3/stm32/rcc.h> // real-time control clock library
#include <libopencm3/stm32/gpio.h> // general purpose input output library
#include <libopencm3/stm32/usart.h> // universal synchronous asynchronous receiver transmitter library
#include <libopencm3/stm32/timer.h> // timer utilities
#include <libopencm3/cm3/nvic.h> // interrupt handler
#include <libopencmsis/core_cm3.h> // Cortex M3 utilities

#include "sensor_sdm120.h" // SDM120 electricity meter header and definitions
#include "global.h" // common methods

/** @defgroup sensor_sdm120_usart USART peripheral used for communication with electricity meter
 *  @{
 */
#define SENSOR_SDM120_USART 3 /**< USART peripheral */
/** @} */

/** @defgroup sensor_sdm120_gpio GPIO peripheral used for controlling RS-485 adapter
 *  @note driver output is enabled on high while receiver output is enabled on low, thus one pin can be used to control both
 *  @{
 */
#define SENSOR_SDM120_REDE_PORT B /**< GPIO port for RS-485 receiver and driver output enable signal */
#define SENSOR_SDM120_REDE_PIN 1 /**< GPIO pin for RS-485 receiver and driver output enable signal */
/** @} */

/** @defgroup sensor_sdm120_timer timer peripheral to enforce waiting time between messages
 *  @note 60 ms are recommended between messages in SDM630 ModBus protocol implementation and this seem to also apply to SDM120
 *  @{
 */
#define SENSOR_SDM120_TIMER 3 /**< timer number to count time */
/** @} */

/* input and output ring buffer, indexes, and available memory */
static uint8_t rx_buffer[9] = {0}; /**< buffer for received response (ModBus response messages can be 2+256+2 long but we will only read up to 2 registers) */
static volatile uint8_t rx_used = 0; /**< number of received data bytes in buffer */
static uint8_t tx_buffer[13] = {0}; /**< buffer for request to transmit (ModBus request messages can be 7+256+2 long but we will only write up to 2 registers */
static volatile uint8_t tx_used = 0; /**< number of byte to transmit */

volatile bool sensor_sdm120_measurement_received = false;

/** the ModBus timeouts to respect for sending messages **/
static enum timeout_t {
	TIMEOUT_BEGIN = 0, /**< silent time before sending data */
	TIMEOUT_END, /**< silent time after sending data */
	TIMEOUT_BETWEEN, /**< time to wait between messages */
	TIMEOUT_MAX /**< last element (useful to no the number of elements) */
} timeout; /**< the current timeout used */
/** current timeout used */
static uint16_t timeout_times[TIMEOUT_MAX] = {0};

/** SDM120 3xxxx input register start addresses for the measurement types */ 
static const uint16_t register_input[] = {
    0x0000, // 30001 voltage (in volts)
	0x0006, // 30007 current (in amperes)
	0x000c, // 30013 active power (in watts)
	0x0012, // 30019 apparent power (in volt amperes)
	0x0018, // 30025 reactive power (in volt amperes reactive)
	0x001e,	// 30031 power factor (0-1)
	0x0046, // 30071 frequency (in hertz)
	0x0048, // 30073 import active energy (in kWh)
	0x004a, // 30075 export active energy (in kWh)
	0x004c, // 30077 import reactive energy (in kVArh)
	0x004e, // 30079 export reactive energy (in kVArh)
	0x0156, // 30343 total active energy (in kWh)
	0x0158  // 30345 total reactive energy (in kVArh)
};

/** SDM120 4xxxx holding register start addresses for the configuration types */
static const uint16_t register_holding[] = {
	0x000c, // relay pulse width (60, 100, or 200 ms)
	0x0012, // network parity stop (0: 1 stop bit no parity, 1: one stop bit even parity, 2: one stop bit odd parity, 3: two stop bits no parity)
	0x0014, // meter slave address (1-247)
	0x001c, // baud rate (0: 2400 bps, 1: 4800 bps, 2: 9600 bps, 5: 1200 bps)
	0x0056, // pulse 1 output mode (1: import active energy, 2: import+export active energy, 4: export active energy, 5: import reactive energy, 6: import+export reactive energy, 8: export reactive energy)
	0xf900, // time of scroll display (0-30 s)
	0xf910, // pulse 1 output (0: 0.001 kWh/imp, 1: 0.01 kWh/imp, 2: 0.1 kWh/imp, 3: 1 kWh/imp)
	0xf920  // measurement mode (1: total=import, 2: total=import+export, 3: total=import-export)
};

/** compute CRC for ModBus
 *  @note ModBus uses ANSi/IBM 16-bits CRC (with normal polynomial 0x8005, reverse polynomial 0xA001, start value 0xfff)
 *  @param[in] buffer data on which to compute the CRC for
 *  @param[in] size number of byte to compute the CRC for
 *  @return computed CRC checksum
 */
static uint16_t crc_modbus(uint8_t* buffer, uint8_t size)
{
	uint16_t crc = 0xffff; // initial value (for ModBus)
	for (uint8_t i=0; i<size; i++) { // go through every byte
		crc ^= (uint16_t)buffer[i]; // XOR byte
		for (uint8_t b=0; b<8; b++) { // go through every bit
			if (crc&0x0001) { // least significant bit is set (we are using the reverse way)
				crc = (crc>>1)^0xA001; // // shift to the right (for the next bit) and XOR with (reverse) polynomial
			} else {
				crc >>= 1; // just shift right (for the next bit)
			}
		}
	}
	return crc;
}

void sensor_sdm120_setup(uint32_t baudrate)
{
	// enable USART I/O peripheral
	rcc_periph_clock_enable(RCC_AFIO); // enable pin alternate function (USART)
	rcc_periph_clock_enable(USART_PORT_RCC(SENSOR_SDM120_USART)); // enable clock for USART port peripheral
	rcc_periph_clock_enable(USART_RCC(SENSOR_SDM120_USART)); // enable clock for USART peripheral
	gpio_set_mode(USART_PORT(SENSOR_SDM120_USART), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, USART_PIN_TX(SENSOR_SDM120_USART)); // setup GPIO pin USART transmit
	gpio_set_mode(USART_PORT(SENSOR_SDM120_USART), GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, USART_PIN_RX(SENSOR_SDM120_USART)); // setup GPIO pin USART receive
	gpio_clear(USART_PORT(SENSOR_SDM120_USART), USART_PIN_RX(SENSOR_SDM120_USART)); // pull down to avoid noise when not connected (it will be set low by RS485 chip when RO is enabled)

	// setup USART parameters for electricity meter
	usart_set_baudrate(USART(SENSOR_SDM120_USART), baudrate); // get baud rate by scrolling through the measurements on the electricity meter's screen (default 2400)
	usart_set_databits(USART(SENSOR_SDM120_USART), 8);
	usart_set_stopbits(USART(SENSOR_SDM120_USART), USART_STOPBITS_1);
	usart_set_mode(USART(SENSOR_SDM120_USART), USART_MODE_TX_RX);
	usart_set_parity(USART(SENSOR_SDM120_USART), USART_PARITY_NONE); // get parity by scrolling through the measurements on the electricity meter's screen (default none)
	usart_set_flow_control(USART(SENSOR_SDM120_USART), USART_FLOWCONTROL_NONE);

	nvic_enable_irq(USART_IRQ(SENSOR_SDM120_USART)); // enable the USART interrupt
	usart_enable_rx_interrupt(USART(SENSOR_SDM120_USART)); // enable receive interrupt
	usart_enable(USART(SENSOR_SDM120_USART)); // enable USART

	// setup GPIO
	rcc_periph_clock_enable(RCC_GPIO(SENSOR_SDM120_REDE_PORT)); // enable clock for GPIO peripheral
	gpio_set_mode(GPIO(SENSOR_SDM120_REDE_PORT), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO(SENSOR_SDM120_REDE_PIN)); // setup GPIO pin for receiver and driver output enable pin
	gpio_clear(GPIO(SENSOR_SDM120_REDE_PORT),GPIO(SENSOR_SDM120_REDE_PIN)); // disable driver output and enable receive output

	// setup timer to wait for minimal time before next transmission
	rcc_periph_clock_enable(RCC_TIM(SENSOR_SDM120_TIMER)); // enable clock for timer block
	timer_reset(TIM(SENSOR_SDM120_TIMER)); // reset timer state
	timer_set_mode(TIM(SENSOR_SDM120_TIMER), TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP); // set timer mode, use undivided timer clock,edge alignment (simple count), and count up
	timer_one_shot_mode(TIM(SENSOR_SDM120_TIMER)); // stop counter after update event (we only need to count down once)
	timer_set_prescaler(TIM(SENSOR_SDM120_TIMER), 66-1); // set the prescaler so this 16 bits timer allows to wait for 60 ms ( 1/(72E6/66/(2**16))=60.07ms )
	timeout_times[TIMEOUT_BEGIN] = (rcc_ahb_frequency/(TIM_PSC(TIM(SENSOR_SDM120_TIMER))+1))/baudrate/8/2.5; // wait at least 2.5 characters before sending data
	timeout_times[TIMEOUT_END] = (rcc_ahb_frequency/(TIM_PSC(TIM(SENSOR_SDM120_TIMER))+1))/baudrate/8/2.5; // wait at least 2.5 characters after sending data
	timeout_times[TIMEOUT_BETWEEN] = 0.06*(rcc_ahb_frequency/(TIM_PSC(TIM(SENSOR_SDM120_TIMER))+1)); // wait at least 60 ms before sending the next message
	timer_clear_flag(TIM(SENSOR_SDM120_TIMER), TIM_SR_UIF); // clear flag
	timer_enable_irq(TIM(SENSOR_SDM120_TIMER), TIM_DIER_UIE); // enable update interrupt for timer
	nvic_enable_irq(NVIC_TIM_IRQ(SENSOR_SDM120_TIMER)); // catch interrupt in service routine

	// reset states
	tx_used = 0;
	rx_used = 0;
	sensor_sdm120_measurement_received = false;
}

/** send request to electricity meter
 *  @param[in] meter_id electricity meter device id (ModBus salve address)
 *  @param[in] function ModBus function: 0x03 read two 16 bits holding registers, 0x04 read two 16 bits input registers, 0x10 write two 16 bits holding registers
 *  @param[in] address register start point address
 *  @param[in] value value to store in holding register (if function 0x10 is used)
 *  @return if request is correct and transmission started
 */
static bool sensor_sdm120_transmit_request(uint8_t meter_id, uint8_t function, uint16_t address, float value)
{
	if (meter_id==0) { // broadcast request are not supported
		return false;
	}
	if (function!=0x03 && function!=0x04 && function!=0x10) { // function not supported
		return false;
	}
	if (address%2) { // even register addresses are not supported by device
		return false;
	}
	while (tx_used) { // transmission is ongoing
		__WFI(); // wait until something happens (transmission ended)
	}
	// build request packet
	uint8_t packet[11]; // buffer to build ModBus message (without error check)
	uint8_t packet_size = 0; // ModBus message size (without error check)
	packet[0] = meter_id; // set slave device address
	packet[1] = function; // set function
	packet[2] = address>>8; // set high register address
	packet[3] = address; // set low register address
	packet[4] = 0; // set high number of registers to read
	packet[5] = 2; // set low number of register to read (the measurement are encoded using 32 bits IEE745 float, and register hold 16 bits, thus we want to read 2 registers
	if (function==0x03 || function==0x04) { // read register
		packet_size = 6; // set message size
	} else if (function==0x10) { // write register
		packet[6] = 4; // byte count (writing two 16 bits registers)
		// store little endian encoded value in big endian encoded data
		uint8_t* data = (uint8_t*)&value;
		packet[7] = data[3];
		packet[8] = data[2];
		packet[9] = data[1];
		packet[10] = data[0];
		packet_size = 11; // set message size
	}
	uint16_t crc = crc_modbus(packet, packet_size); // compute error check
	for (uint8_t i=0; i<packet_size; i++) {
		tx_buffer[packet_size-i+1] = packet[i]; // copy packet to tx buffer in reverse order (this is how sending is implemented)
	}
	tx_buffer[1] = crc; // set low error check
	tx_buffer[0] = crc>>8; // set high error check
	tx_used = packet_size+2; // set request size
	rx_used = 0; // reset reset buffer
	sensor_sdm120_measurement_received = false; // reset measurement flag
	while (TIM_CR1(TIM(SENSOR_SDM120_TIMER))&TIM_CR1_CEN) { // timer is already used
		 __WFI(); // wait until something happens (timer is available again)
	}	
	gpio_set(GPIO(SENSOR_SDM120_REDE_PORT),GPIO(SENSOR_SDM120_REDE_PIN)); // enable driver output and disable receive output
	// start timeout
	timeout = TIMEOUT_BEGIN; // select time before sending message
	timer_set_period(TIM(SENSOR_SDM120_TIMER), timeout_times[timeout]); // set corresponding timeout
	timer_set_counter(TIM(SENSOR_SDM120_TIMER), 0); // reset timer counter to get preset waiting time
	timer_enable_counter(TIM(SENSOR_SDM120_TIMER)); // wait

	return true;
}

bool sensor_sdm120_measurement_request(uint8_t meter_id, enum sensor_sdm120_measurement_type_t type)
{
	if (type>=SENSOR_SDM120_MEASUREMENT_MAX) { // invalid type
		return false;
	}
	return sensor_sdm120_transmit_request(meter_id, 0x04, register_input[type], 0);
}

bool sensor_sdm120_configuration_request(uint8_t meter_id, enum sensor_sdm120_configuration_type_t type)
{
	if (type>=SENSOR_SDM120_CONFIGURATION_MAX) { // invalid type
		return false;
	}
	return sensor_sdm120_transmit_request(meter_id, 0x03, register_holding[type], 0);
}

bool sensor_sdm120_configuration_set(uint8_t meter_id, enum sensor_sdm120_configuration_type_t type, float value)
{
	if (type>=SENSOR_SDM120_CONFIGURATION_MAX) { // invalid type
		return false;
	}
	return sensor_sdm120_transmit_request(meter_id, 0x10, register_holding[type], value);
}

float sensor_sdm120_measurement_decode(void)
{
	float measurement = NAN; // decoded measurement to return (invalid in the beginning)
	if (!sensor_sdm120_measurement_received) { // no measurement received
		return NAN;
	} else {
		sensor_sdm120_measurement_received = false; // reset flag
	}
	if (rx_used<5) { // not a complete response (minimum is address, function, size/error, error check low, error check high)
		return NAN;
	}
	// a complete message has been received
	if (crc_modbus(rx_buffer,rx_used)) { // checksum error, error check failed
		measurement = NAN;
	} else if (rx_buffer[1]&0x80) { // error condition received
		measurement = INFINITY; // indicate we received and error
	} else {
		switch (rx_buffer[1]) {
			case 0x03: // read 4xxx holding register response received
			case 0x04: // read 3xxxx input register response received
				if (rx_buffer[2]==0x04 && rx_used>=(4+5)) { // 2 registers received, corresponds to implemented request
					// convert big endian received float value to little endian return value
					uint8_t* convert = (uint8_t*)&measurement;
					convert[0] = rx_buffer[6];
					convert[1] = rx_buffer[5];
					convert[2] = rx_buffer[4];
					convert[3] = rx_buffer[3];
				}
				break;
			case 0x10: // write 4xxx holding register response received
				measurement = (rx_buffer[4]<<8)+rx_buffer[5]; // number of registers written
				break; // not supported currently
			default: // unknown function response received
				measurement = INFINITY;
				break; // nothing to do
		}
	}
	rx_used = 0; // reset rx_buffer usage
	return measurement;
}
	
/** USART interrupt service routine called when data has been transmitted or received */
void USART_ISR(SENSOR_SDM120_USART)(void)
{
	if (usart_get_interrupt_source(USART(SENSOR_SDM120_USART), USART_SR_TXE)) { // data has been transmitted
		if (tx_used) { // not all bytes transmitted
			usart_send(USART(SENSOR_SDM120_USART),tx_buffer[--tx_used]); // transmit next byte (clears flag)
		} else { // all bytes transmitted
			usart_disable_tx_interrupt(USART(SENSOR_SDM120_USART)); // disable transmit interrupt
			USART_SR(USART(SENSOR_SDM120_USART)) &= ~USART_SR_TXE; // clear flag
			USART_CR1(USART(SENSOR_SDM120_USART)) |= USART_CR1_TCIE; // enable transfer complete interrupt
		}
	}
	if (usart_get_interrupt_source(USART(SENSOR_SDM120_USART), USART_SR_TC)) { // data has been completely transmitted
		USART_CR1(USART(SENSOR_SDM120_USART)) |= USART_CR1_TCIE; // disable transfer complete interrupt
		USART_SR(USART(SENSOR_SDM120_USART)) &= ~USART_SR_TC; // clear flag
		timeout = TIMEOUT_END; // select wait time after sending data
		timer_set_period(TIM(SENSOR_SDM120_TIMER), timeout_times[timeout]); // set corresponding timeout
		timer_set_counter(TIM(SENSOR_SDM120_TIMER), 0); // reset timer counter to get preset waiting time
		timer_enable_counter(TIM(SENSOR_SDM120_TIMER)); // wait
	}
	if (usart_get_interrupt_source(USART(SENSOR_SDM120_USART), USART_SR_RXNE)) { // data has been received
		if (gpio_get(GPIO(SENSOR_SDM120_REDE_PORT),GPIO(SENSOR_SDM120_REDE_PIN))) { // not in receiver mode
			USART_SR(USART(SENSOR_SDM120_USART)) &= ~USART_SR_RXNE; // clear flag, ignore received data
		} else if (rx_used<LENGTH(rx_buffer)) { // receiving response
			rx_buffer[rx_used++] = usart_recv(USART(SENSOR_SDM120_USART)); // put received byte in buffer (clears flag)
			if (rx_used==1 && rx_buffer[0]==0) { // this is wrong decoding because the signal is going low on idle, which is misinterpreted as start bit (and the 0 broadcast device address is not supported by this device)
				rx_used = 0; // reset buffer
			} else if (rx_used>=5 && (rx_buffer[1]&0x80)) { // error condition response received
				sensor_sdm120_measurement_received = true; // notify used response has been received
			} else if (rx_used>=5 && (uint8_t)(rx_used-5)>=rx_buffer[2] && (rx_buffer[1]==0x04 || rx_buffer[1]==0x03)) { // read input or holding register response received
				sensor_sdm120_measurement_received = true; // notify used response has been receive
			} else if (rx_used>=8 && rx_buffer[1]==0x10) { // write holding register response received
				sensor_sdm120_measurement_received = true; // notify used response has been receive
			}
		} else { // buffer full and unknown response received
			USART_SR(USART(SENSOR_SDM120_USART)) &= ~USART_SR_RXNE; // clear flag (wait for user to read measurement, this clears the buffer)
		}
		timeout = TIMEOUT_END; // select time after receiving data
		timer_set_period(TIM(SENSOR_SDM120_TIMER), timeout_times[timeout]); // set corresponding timeout
		timer_set_counter(TIM(SENSOR_SDM120_TIMER), 0); // reset timer counter to get preset waiting time
		timer_enable_counter(TIM(SENSOR_SDM120_TIMER)); // wait
	}
}

/** interrupt service routine called on timeout */
void TIM_ISR(SENSOR_SDM120_TIMER)(void)
{
	if (timer_get_flag(TIM(SENSOR_SDM120_TIMER), TIM_SR_UIF)) { // update event happened
		timer_clear_flag(TIM(SENSOR_SDM120_TIMER), TIM_SR_UIF); // clear flag
		// because of the one pulse mode the timer is stopped automatically
		switch (timeout) { // timeout before action passed
			case (TIMEOUT_BEGIN): // we can now send the data
				USART_SR(USART(SENSOR_SDM120_USART)) &= USART_SR_TXE; // clear interrupt flag
				usart_enable_tx_interrupt(USART(SENSOR_SDM120_USART)); // enable interrupt to send other bytes
				usart_send(USART(SENSOR_SDM120_USART),tx_buffer[--tx_used]); // start transmission
				break;
			case (TIMEOUT_END): // we now have to wait before sending the next message
		        gpio_clear(GPIO(SENSOR_SDM120_REDE_PORT),GPIO(SENSOR_SDM120_REDE_PIN)); // disable driver output (and enable receive output)
				timeout = TIMEOUT_BETWEEN; // select time between sending message
				timer_set_period(TIM(SENSOR_SDM120_TIMER), timeout_times[timeout]); // set corresponding timeout
				timer_set_counter(TIM(SENSOR_SDM120_TIMER), 0); // reset timer counter to get preset waiting time
				timer_enable_counter(TIM(SENSOR_SDM120_TIMER)); // wait
			case (TIMEOUT_BETWEEN): // nothing to do, we are allowed to send the next message
				break;
			default:
				break;
		}
	}
}

