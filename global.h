/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/** global definitions and methods (API)
 *  @file global.h
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @date 2016
 */
#pragma once

/** enable debugging functionalities */
#define DEBUG false

/** get the length of an array */
#define LENGTH(x) (sizeof(x) / sizeof((x)[0]))
/** concatenate 2 arguments (used to defer concatenation) */
#define CAT2(x,y) x##y
/** concatenate 3 arguments (used to defer concatenation) */
#define CAT3(x,y,z) x##y##z

/** @defgroup reg_macro macros to  define values based on other defines values
 *  @note used when the value is calculated or isn't a value
 *  @{
 */
/** get GPIO based on GPIO identifier */
#define GPIO(x) CAT2(GPIO,x)
/** get GPIO based on identifier */
#define GPIO_(x) CAT2(GPIO_,x)
/** get GPIO based on bank identifier */
#define GPIO_BANK_(x) CAT2(GPIO_BANK_,x)
/** get RCC for GPIO based on GPIO identifier */
#define RCC_GPIO(x) CAT2(RCC_GPIO,x)
/** get TIM based on TIM identifier */
#define TIM(x) CAT2(TIM,x)
/** get RCC for timer based on TIM identifier */
#define RCC_TIM(x) CAT2(RCC_TIM,x)
/** get NVIC IRQ for timer base on TIM identifier */
#define NVIC_TIM_IRQ(x) CAT3(NVIC_TIM,x,_IRQ)
/** get interrupt service routine for timer base on TIM identifier */
#define TIM_ISR(x) CAT3(tim,x,_isr)
/** get external interrupt based on pin identifier */
#define EXTI(x) CAT2(EXTI,x)
/** get NVIC IRQ for external interrupt base on external interrupt/pin */
#define NVIC_EXTI_IRQ(x) CAT3(NVIC_EXTI,x,_IRQ)
#define NVIC_EXTI5_IRQ NVIC_EXTI9_5_IRQ /**< IRQ for line 9 to 5 for pin 5 */
#define NVIC_EXTI6_IRQ NVIC_EXTI9_5_IRQ /**< IRQ for line 9 to 5 for pin 6 */
#define NVIC_EXTI7_IRQ NVIC_EXTI9_5_IRQ /**< IRQ for line 9 to 5 for pin 7 */
#define NVIC_EXTI8_IRQ NVIC_EXTI9_5_IRQ /**< IRQ for line 9 to 5 for pin 8 */
#define NVIC_EXTI9_IRQ NVIC_EXTI9_5_IRQ /**< IRQ for line 9 to 5 for pin 9 */
#define NVIC_EXTI10_IRQ NVIC_EXTI15_10_IRQ /**< IRQ for line 15 to 10 for pin 10 */
#define NVIC_EXTI11_IRQ NVIC_EXTI15_10_IRQ /**< IRQ for line 15 to 10 for pin 11 */
#define NVIC_EXTI12_IRQ NVIC_EXTI15_10_IRQ /**< IRQ for line 15 to 10 for pin 12 */
#define NVIC_EXTI13_IRQ NVIC_EXTI15_10_IRQ /**< IRQ for line 15 to 10 for pin 13 */
#define NVIC_EXTI14_IRQ NVIC_EXTI15_10_IRQ /**< IRQ for line 15 to 10 for pin 14 */
#define NVIC_EXTI15_IRQ NVIC_EXTI15_10_IRQ /**< IRQ for line 15 to 10 for pin 15 */
/** get interrupt service routine for timer base on external interrupt/pin */
#define EXTI_ISR(x) CAT3(exti,x,_isr)
#define exti5_isr exti9_5_isr /**< isr for line 9 to 5 for pin 5 */
#define exti6_isr exti9_5_isr /**< isr for line 9 to 5 for pin 6 */
#define exti7_isr exti9_5_isr /**< isr for line 9 to 5 for pin 7 */
#define exti8_isr exti9_5_isr /**< isr for line 9 to 5 for pin 8 */
#define exti9_isr exti9_5_isr /**< isr for line 9 to 5 for pin 9 */
#define exti10_isr exti15_10_isr /**< isr for line 15 to 10 for pin 10 */
#define exti11_isr exti15_10_isr /**< isr for line 15 to 10 for pin 11 */
#define exti12_isr exti15_10_isr /**< isr for line 15 to 10 for pin 12 */
#define exti13_isr exti15_10_isr /**< isr for line 15 to 10 for pin 13 */
#define exti14_isr exti15_10_isr /**< isr for line 15 to 10 for pin 14 */
#define exti15_isr exti15_10_isr /**< isr for line 15 to 10 for pin 15 */
/** get USART based on USART identifier */
#define USART(x) CAT2(USART,x)
/** get RCC for USART based on USART identifier */
#define USART_RCC(x) CAT2(RCC_USART,x)
/** get NVIC IRQ for USART based on USART identifier */
#define USART_IRQ(x) CAT3(NVIC_USART,x,_IRQ)
/** get interrupt service routine for USART based on USART identifier */
#define USART_ISR(x) CAT3(usart,x,_isr)
/** get port for USART based on USART identifier */
#define USART_PORT(x) CAT2(USART_PORT,x)
#define USART_PORT1 GPIOA /**< USART 1 is on port A */
#define USART_PORT2 GPIOA /**< USART 2 is on port A */
#define USART_PORT3 GPIOB /**< USART 3 is on port B */
/** get RCC for USART port based on USART identifier */
#define USART_PORT_RCC(x) CAT2(RCC_USART_PORT,x)
#define RCC_USART_PORT1 RCC_GPIOA /**< USART 1 is on port A */
#define RCC_USART_PORT2 RCC_GPIOA /**< USART 2 is on port A */
#define RCC_USART_PORT3 RCC_GPIOB /**< USART 3 is on port B */
/** get transmit pin for USART based on USART identifier */
#define USART_PIN_TX(x) CAT3(GPIO_USART,x,_TX)
/** get receive pin for USART based on USART identifier */
#define USART_PIN_RX(x) CAT3(GPIO_USART,x,_RX)
/** @} */

/** @defgroup board_led board LED GPIO
 *  @{
 */
#if defined(SYSTEM_BOARD)
/* on system board LED is on pin 11/PA1 */
#define LED_PORT A /**< GPIO port (port A on system board) */
#define LED_PIN	1 /**< GPIO pin (pin PA1 on system board) */
#elif defined(BLUE_PILL)
/* on minimum system LED is on pin 2/PC13 */
#define LED_PORT C /**< GPIO port (port C on blue pill) */
#define LED_PIN 13 /**< GPIO pin (pin PC13 on system board) */
#elif defined (MAPLE_MINI)
/* on maple mini LED is on pin 19/PB1 */
#define LED_PORT B /**< GPIO port (port B on maple mini) */
#define LED_PIN 1 /**< GPIO pin (pin PB1 on maple mini) */
#endif
/** @} */

/** @defgroup board_button board user button GPIO
 *  @{
 */
#if defined(MAPLE_MINI)
/* on maple mini user button is on 32/PB8 */
#define BUTTON_PORT B /**< GPIO port (port B on maple mini) */
#define BUTTON_PIN 8 /**< GPIO pin (pin PB8 on maple mini) */
#endif
/** @} */

extern volatile bool button_flag; /**< flag set when board user button has been pressed/released */

/** default printf output */
int _write(int file, char *ptr, int len);

/** get binary representation of a number
 *  @param[in] binary number to represent in binary
 *  @param[in] rjust justify representation with leading zeros
 *  @return string with binary representation of the number
 */
char* b2s(uint64_t binary, uint8_t rjust);

/** switch on board LED */
void led_on(void);

/** switch off board LED */
void led_off(void);

/** toggle board LED */
void led_toggle(void);

/** setup board peripherals */
void board_setup(void);

